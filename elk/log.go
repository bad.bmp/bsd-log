package elk

import (
	"encoding/json"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
)

const LOG_LEVEL_INFO int = 0
const LOG_LEVEL_DEBUG int = 1
const LOG_LEVEL_WARN int = 2
const LOG_LEVEL_FATAL int = 3
const DefaultTimestampFormat = time.RFC3339

func SaveLog(level int, team string, logSuffix string, jsonData string, msg string) error {
	x := make(map[string]interface{})
	if jsonData != "" {
		err := json.Unmarshal([]byte(jsonData), &x)
		if err != nil {
			return err
		}
	}

	log.SetFormatter(&log.JSONFormatter{})
	log.SetOutput(os.Stdout)
	//log.SetLevel(log.InfoLevel)
	fields := log.Fields{
		"@team":    team,
		"@suffix ": logSuffix,
	}
	for ia, va := range fields {
		if va != "" {
			x[ia] = va
		}

	}
	fields = x //append(fields, mLog)
	outLog := log.WithFields(fields)

	switch level {
	case LOG_LEVEL_INFO:
		outLog.Infoln(msg)
		break
	case LOG_LEVEL_DEBUG:
		outLog.Debugln(msg)
		break
	case LOG_LEVEL_WARN:
		outLog.Warnln(msg)
		break
	case LOG_LEVEL_FATAL:
		outLog.Fatalln(msg)
		break
	default:
		outLog.Infoln(msg)
		break
	}

	return nil

}
