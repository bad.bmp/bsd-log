package elk

import "testing"

func TestSaveLog(t *testing.T) {
	err := SaveLog(LOG_LEVEL_INFO, "bsd", "suffix", "{\"first\": \"Tom\", \"last\": \"Anderson\"}", "")
	err = SaveLog(LOG_LEVEL_INFO, "bsd", "suffix", "", "")
	if err != nil {
		t.Errorf(err.Error())
	}
}
