package main

import (
	"fmt"

	"gitlab.com/bad.bmp/bsd-log/elk"
)

func main() {
	elk.SaveLog(elk.LOG_LEVEL_INFO, "", "suffix", "{\"first\": \"Tom\", \"last\": \"Anderson\"}", "")
	err := elk.SaveLog(elk.LOG_LEVEL_INFO, "bsd", "suffix", "", "")
	if err != nil {
		fmt.Println(err.Error())
	}
}
