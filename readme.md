#How to get this Module

##Go Get :
go get -insecure gitlab.com/bad.bmp/bsd-log@master
###set global .gitconfig //C:\Users\{username}\.gitconfig
```bash
[url "https://bad.bmp:qJYfuN8HZ7yxSJJ23D-M@gitlab.com/bad.bmp/bsd-log"]
	insteadOf = https://gitlab.com/bad.bmp/bsd-log
```


#Add By dep :

##Preparing :
```bash
dep init
```

###edit file Gopkg.toml
```bash
[[constraint]]
  name = "gitlab.com/bad.bmp/bsd-log"
  branch = "master"
  source = "https://bad.bmp:qJYfuN8HZ7yxSJJ23D-M@gitlab.com/bad.bmp/bsd-log.git"
```

```bash
dep ensure -add gitlab.com/bad.bmp/bsd-log/elk
```

====================================================================================

#How to use ELK LOG

##Method : elk.SaveLog()
```bash
SaveLog(log_level int, team_name string,  logSuffix string, jsonData string, msg string)
```
```bash
Return error or nil(no error)
```

###LOG Level :
```bash
const LOG_LEVEL_INFO int = 0
const LOG_LEVEL_DEBUG int = 1
const LOG_LEVEL_WARN int = 2
const LOG_LEVEL_FATAL int = 3
```
#EXAMPLE
/////////////////////////////////////
```golang
package main

import (
	"fmt"

	"gitlab.com/bad.bmp/bsd-log/elk"
)

func main() {

	err := elk.SaveLog(elk.LOG_LEVEL_INFO, "bsd", "monitor", "{\"first\": \"Tom\", \"last\": \"Anderson\"}", "")
	
	if err != nil {
		fmt.Println(err)
	}
}
```
///////////////////////////////////////////
#Output
```bash
{"@namespace":"newscenter","@suffix ":"monitor","@team":"bsd","first":"Tom","last":"Anderson","level":"info","msg":"","time":"2020-02-12T17:03:13+07:00"}
```



```bash
Dear All

	ข้อมูลของ Format Log ขึ้น Elasticsearch ครับ 

	ELK index - namespace: dcbgw
    Mandatory Fields
       1. @timestamp (ISO8601 with timezone offset) Ex. 2019-03-04T13:52:13+07:00
       2. @team use for grouping index name Ex. mvp, omx, etc. (PS. should be relate with dev team)
       3. @suffix use for separate index name Ex. monitor, console, transaction (PS. Can be type of document)
       4. Base index name will be base on {@team}-{NAMESPACE}-{@suffix}
       5. tags use for separate type of document or group of document with same purpose. use for Search. Ex. monitor, console, transaction (This can be multiple values)

       
       Example
                namespace = mcc, mpf
                @team = mvp
                @suffix = monitor
             
                Example Index name:  k8stdev-mvp-mcc-monitor.YYYY.MM
                                                  k8stdev-mvp-mpf-monitor.YYYY.MM
       PS. @team and @suffix will remove after process into Index name, so if you want to search document with specific data in @team or @suffix please put it into tags instead.
Noted: Dynamic index name with @team and @suffix already implement in k8stdev so feel free to add @team, @suffix to grouping or separate index, if there is no @team, @suffix appear in document Index will be k8stdev-{NAMESPACE}.YYYY.MM


Best Regards,
Thanaphat Nuangjumnong(Pump)
Dep. Enterprise Service Development / System & Platform Management
Mobile :  +66 (0) 91-576-9938
```